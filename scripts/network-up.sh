bootnode -addr 127.0.0.1:30301 -verbosity 9 -nodekey boot.key > bootnode.log 2>&1 &
sleep 2
key=$(grep -o 'enode://.*' bootnode.log)
echo $key
geth --rpc --rpcaddr 127.0.0.1 --rpcport 8545 --rpccorsdomain 'http://127.0.0.1:8000' --rpcapi="db,eth,net,web3,personal,web3" --datadir data/master --networkid 15 --port 30302 --bootnodes $key --mine --minerthreads=1 --etherbase 7df9a875a174b3bc565e6424a0050ebc1b2d1d82 > master.log 2>&1 &
geth --rpc --rpcaddr 127.0.0.1 --rpcport 8546 --rpccorsdomain 'http://127.0.0.1:8000' --rpcapi="db,eth,net,web3,personal,web3" --datadir data/iot --networkid 15 --port 30303 --bootnodes $key > iot.log 2>&1 &
geth --rpc --rpcaddr 127.0.0.1 --rpcport 8547 --rpccorsdomain 'http://127.0.0.1:8000' --rpcapi="db,eth,net,web3,personal,web3" --datadir data/user --networkid 15 --port 30304 --bootnodes $key > user.log 2>&1 &
