import sys
import subprocess

contract_file = '../contracts/caf'
contract_name = 'Caf'
source = "d834238bc0cd57386f4a8426e0a584f9fa20dd53"
addr = 'd834238bc0cd57386f4a8426e0a584f9fa20dd53'
gas = 2100000
amount = '10000000000000000000'
transactions = []
def insert_contract(args):
  subprocess.call(('./solc-static-linux --abi --optimize --overwrite -o abis %s' % contract_file).split())
  subprocess.call(('./solc-static-linux --bin --optimize --overwrite -o bins %s' % contract_file).split())
  subprocess.call(("cp abis/%s.abi ../web/abis/coffee.json" % contract_name).split())
  abi = open('abis/' + contract_name + ".abi").read()
  binary = open('bins/' + contract_name + ".bin").read()
  cmd = "personal.unlockAccount('%s','test');eth.contract(%s).new(%s{data:'0x%s',from:'%s',gas:'%s'}).transactionHash;" % (addr, abi, args, binary, addr, gas)
  subprocess.call("geth attach data/master/geth.ipc --exec".split() + [cmd])

rcv1 = sys.argv[1]
rcv2 = sys.argv[2]
for i in range(1):
  insert_contract("%s,%s,%s,%s," % (rcv1, rcv2, amount, amount))
