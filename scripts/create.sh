mkdir data
mkdir data/user
mkdir data/iot
mkdir data/master
rm -rf data/master/geth
rm -rf data/iot/geth
rm -rf data/user/geth
geth --datadir data/master init genesis.json
geth --datadir data/iot init genesis.json
geth --datadir data/user init genesis.json
# future runs
# geth --datadir data/master --networkid 15
bootnode --genkey=boot.key
