$(function() {
  var web3 = new Web3(new Web3.providers.HttpProvider("http://127.0.0.1:8547"));
  web3.eth.getAccounts(function(error, results) {
    console.log(error);
    console.log(results);
    $("#login-wallet-id").val(results[0]);
  });
  window.web3 = web3;
  $("#login-button").click(function() {
    var ret = web3.personal.unlockAccount($("#login-wallet-id").val(), $("#login-password").val());
    console.log(ret); // Handle errors
  });
  $("#send-button").click(function() {
    var ret = web3.eth.sendTransaction({
      from: $("#login-wallet-id").val(),
      to: $("#send-wallet-id").val(),
      value: $("#send-amount").val(),
      gas: 2100000
    });
    console.log(ret);
  });
  $("#balance-button").click(function() {
    $("#current-balance").val(web3.eth.getBalance($("#login-wallet-id").val()));
  });
  $("#port-pick-button").click(function() {
    web3.setProvider(new Web3.providers.HttpProvider("http://127.0.0.1:" + $("#port-pick").val()));
    web3.eth.getAccounts(function(error, results) {
      console.log(error);
      console.log(results);
      $("#login-wallet-id").val(results[0]);
    });
  });
});
