// Once a button is pressed: Update QR code that encodes address and prize.
// Also display in the text field.
// Modest timeout, the reset.
// Highlight that button.
// If event is received, blink button, then reset and log.

$(function () {
var config = {
  abi_url: "/abis/coffee.json",
  meta_url: "/metas/coffee.json",
  rpc_endpoint: "http://127.0.0.1:8546",
  contract_address: "0x1e6bc59d62a2686e921c9013b3ce11ac987c1a24",
  iot_address: "0x8f241d12444afdd2e6da0aa7c9a2e9f16e9cd48d",
  iot_password: 'test'
};
var contract_abi = null;
$.ajax({
  url: config.abi_url,
  async: false,
  type: 'GET',
  success: function(r) {
    contract_abi = r;
  },
  error: function(e) {
    console.log(e);
  }
});
console.log(contract_abi);

var contract_meta = null;
$.ajax({
  url: config.abi_url,
  async: false,
  type: 'GET',
  success: function(r) {
    contract_abi = r;
  },
  error: function(e) {
    console.log(e);
  }
});
console.log(contract_meta);

var web3 = new Web3(new Web3.providers.HttpProvider(config.rpc_endpoint));
web3.personal.unlockAccount(config.iot_address, config.iot_password);
var contract = web3.eth.contract(contract_abi).at(config.contract_address);

//var contract_abi = null;
//$.ajax({
//  url: config.abi_url,
//  async: false,
//  type: 'GET',
//  success: function(r) {
//    contract_abi = r;
//  },
//  error: function(e) {
//    console.log(e);
//  }
//});
var timer = null;
var current_button = null;

var reset = function() {
  $("#display h1").text("We love to entertain you!");
  $("#display p").text("What would you like to watch?");
  $("#display img").css("display", "none");
};

$("button.order").click(function() {
  console.log("A");
  //if (timer) clearTimeout(timer);
  //timer = setTimeout(function() {
  //  contract.cancel();
  //}, 5000);
  if (current_button) current_button.removeClass("btn-info").addClass("btn-warning");
  $(this).removeClass("btn-warning").addClass("btn-info");
  current_button = $(this);
  $("#display h1").text("Coming right up...");
  $("#display p").text("Please pay " + $(this).data('price') + ' ETH to ' + config.contract_address + '.');
  $("#display img").css("display", "block");
});

contract.Failed().watch(function(err, item) {
  current_button.removeClass("btn-info").addClass("btn-warning");
  current_button = null;
  $("#status").prepend("<p>Order Failed: First Choice</p>");
  $("#status p").slice(4).remove();
  $("#display h1").text("Oops");
  $("#display p").text("Something went wrong with your order. If you already payed, you'll be refunded soon.");
  setTimeout(reset, 7000);
});

contract.Success().watch(function(err, result) {
  current_button.removeClass("btn-info").addClass("btn-success");
  var resetBtn = current_button;
  if (current_button.data('timeout')) {
    setTimeout(function() {
      resetBtn.removeClass('btn-success').addClass('btn-warning');
    }, current_button.data('timeout'));
  }
  $("#status").prepend("<p>Order Successful: First Choice</p>");
  $("#status p").slice(4).remove();
  $("#display h1").text("Enjoy!");
  $("#display p").text("The \"" + current_button.text() + "\" is now enabled.");
  setTimeout(reset, 7000);
  current_button = null;
});
});
