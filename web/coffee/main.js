// Once a button is pressed: Update QR code that encodes address and prize.
// Also display in the text field.
// Modest timeout, the reset.
// Highlight that button.
// If event is received, blink button, then reset and log.

$(function () {
var config = {
  abi_url: "/abis/coffee.json",
  meta_url: "/metas/coffee.json",
  rpc_endpoint: "http://127.0.0.1:8546",
  contract_address: "0x1e6bc59d62a2686e921c9013b3ce11ac987c1a24",
  iot_address: "0x8f241d12444afdd2e6da0aa7c9a2e9f16e9cd48d",
  iot_password: 'test'
};
var contract_abi = null;
$.ajax({
  url: config.abi_url,
  async: false,
  type: 'GET',
  success: function(r) {
    contract_abi = r;
  },
  error: function(e) {
    console.log(e);
  }
});
console.log(contract_abi);

var contract_meta = null;
$.ajax({
  url: config.abi_url,
  async: false,
  type: 'GET',
  success: function(r) {
    contract_abi = r;
  },
  error: function(e) {
    console.log(e);
  }
});
console.log(contract_meta);

var web3 = new Web3(new Web3.providers.HttpProvider(config.rpc_endpoint));
web3.personal.unlockAccount(config.iot_address, config.iot_password);
var contract = web3.eth.contract(contract_abi).at(config.contract_address);
//var transfer = function(address, balance) {
//  console.log(address + ":" + balance);
//};
//var contract = (function() {
//  var my_item = null;
//  var last_address = null;
//  var balance = 0;
//  var target = 'receiver';
//  var price = [1,2,3,4,5,6,7,7,8];
//  var handle_failed = function() {};
//  var handle_fullfilled = function() {};
//  var expect_payment_for = function(item) {
//    console.log("C" + item);
//    if (my_item) cancel();
//    my_item = item;
//    needed = price[item];
//  };
//  var cancel = function() {
//    console.log(my_item);
//    if (my_item) {
//      my_item = null;
//      if (balance) transfer(last_address, balance);
//      balance = 0;
//      last_address = null;
//      handle_failed(my_item);
//    }
//  };
//  var receive = function(from, amount) {
//    balance += amount;
//    last_address = from;
//    if (balance >= needed) {
//      transfer(target, balance);
//      handle_fullfilled(my_item);
//      my_item = null;
//      balance = 0;
//      last_address = null;
//      needed = 0;
//    }
//  };
//  return {
//    expect_payment_for: expect_payment_for,
//    cancel: cancel,
//    on_failed: function(h) { handle_failed = h; },
//    on_fullfilled: function(h) { handle_fullfilled = h; },
//    receive: receive
//  };
//})();

window.contract = contract;

var timer = null;
var current_button = null;

var reset = function() {
  $("#display h1").text("Welcome!");
  $("#display p").text("What is your order today?");
  $("#display img").css("display", "none");
};

$("button.order").click(function() {
//  contract.expect_payment_for($(this).data('price'));
  //if (timer) clearTimeout(timer);
  //timer = setTimeout(function() {
  //  contract.Cancel({from: config.iot_address});
  //}, 5000);
  if (current_button) current_button.removeClass("btn-primary").addClass("btn-info");
  $(this).removeClass("btn-info").addClass("btn-primary");
  current_button = $(this);
  $("#display h1").text("Heating up the machine...");
  $("#display p").text("Please pay " + $(this).data('price') + ' ETH to ' + config.contract_address + '.');
  $("#display img").css("display", "block");
});

contract.Failed().watch(function(err, result) {
  current_button.removeClass("btn-primary").addClass("btn-info");
  current_button = null;
  $("#status").prepend("<p>Order Failed: First Choice</p>");
  $("#status p").slice(4).remove();
  $("#display h1").text("Oops");
  $("#display p").text("Something went wrong with your order. If you already payed, you'll be refunded soon.");
  setTimeout(reset, 7000);
});

contract.Success().watch(function(err, result) {
  current_button.removeClass("btn-primary").addClass("btn-success");
  var resetBtn = current_button;
  setTimeout(function() {
    resetBtn.removeClass("btn-success").addClass("btn-info");
  }, 7000);
  $("#status").prepend("<p>Order Successful: First Choice</p>");
  $("#status p").slice(4).remove();
  $("#display h1").text("Enjoy!");
  $("#display p").text("One " + current_button.text() + " coming right up.");
  setTimeout(reset, 7000);
  current_button = null;
});
});
