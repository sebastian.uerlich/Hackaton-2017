$(function() {
  var web3 = [
    new Web3(new Web3.providers.HttpProvider("http://127.0.0.1:8545")),
    new Web3(new Web3.providers.HttpProvider("http://127.0.0.1:8546")),
    new Web3(new Web3.providers.HttpProvider("http://127.0.0.1:8547"))];
  window.web3 = web3;
  var data = [{},{},{}];
  var update = function() {
    for (var i = 0; i < 3; i++) {
      (function(ii) {
        $("#node-" + i).text("");
        web3[i].eth.getAccounts(function(error, results) {
          for (var j = 0; j < results.length; j++) {
            var account = results[j];
            //console.log(web3[i]);
            //console.log(web3[i].eth);
            var bal = 0+web3[ii].eth.getBalance(account);
            $("#node-" + ii).append("<h5>" + account + "</h5>");
            if (! data[ii][j]) data[ii][j] = [];
            var more = true;
            if (data[ii][j].length > 0) {
              if (bal === data[ii][j][0]) more = false;;
            }
            if (more) {
              data[ii][j].unshift(bal);
              data[ii][j] = data[ii][j].slice(0, 10);
            }
            for (var k = 0; k < data[ii][j].length; k++) {
              $("#node-" + ii).append('<div class="data">' + data[ii][j][k] + "</div>");
            }
          }
        });
      })(i);
    }
    setTimeout(update, 1000);
  };
  setTimeout(update, 1000);
});
